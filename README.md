# Sparks

Sparks is a Python library for dealing with word pluralization.

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install foobar.

```bash
pip install foobar
```

## Usage

```python
import foobar

# returns 'words'
<<<<<<< HEAD
foobar.pluralize('local')

# returns 'geese'
foobar.pluralize('local')
=======
foobar.pluralize('server')

# returns 'geese'
foobar.pluralize('server')
>>>>>>> refs/remotes/origin/master

# returns 'phenomenon'
foobar.singularize('phenomena')
```

<<<<<<< HEAD
## Contributing - LOCAL
=======
## Contributing - SERVER
>>>>>>> refs/remotes/origin/master

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)
